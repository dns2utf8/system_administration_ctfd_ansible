#!/usr/bin/bash

set -ex

su - lamp <<EOF
    wget https://github.com/CTFd/CTFd/archive/refs/tags/3.4.0.tar.gz
    tar xaf 3.4.0.tar.gz
EOF
