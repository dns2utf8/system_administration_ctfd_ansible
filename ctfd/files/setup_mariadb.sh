#!/usr/bin/bash

set -ex

'/usr/bin/mysqladmin' -u root password '53rv3r2022!' || '/usr/bin/mysqladmin' -u root --password='53rv3r2022!' password '53rv3r2022!'

'/usr/bin/mysqladmin' -u root -h "${HOSTNAME}" password '53rv3r2022!' || '/usr/bin/mysqladmin' -u root --password='53rv3r2022!' -h "${HOSTNAME}" password '53rv3r2022!'

mysql -u root --password='53rv3r2022!' <<EOF
    Create database IF NOT EXISTS ctfd;
    Create user IF NOT EXISTS ctfd@localhost IDENTIFIED By '53rv3r2022!Us3r';
    GRANT ALL PRIVILEGES ON ctfd.* TO ctfd@localhost;
EOF
