#!/usr/bin/bash

set -ex

su - lamp <<EOF
    python3 -m venv --system-site-packages venv
    source venv/bin/activate
    pip install --upgrade --progress-bar off pip
    
    pip install --progress-bar off -r ./CTFd-3.4.0/requirements.txt
EOF
