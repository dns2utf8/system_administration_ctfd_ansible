import os
import sys
import site

#raise SyntaxError( "(uid={})".format( os.getuid() ) )

# From https://modwsgi.readthedocs.io/en/master/user-guides/virtual-environments.html

python_home = '/srv/ctfd/venv'
installation_home = '/srv/ctfd/CTFd-3.4.0'

#os.chdir(python_home)
os.chdir(installation_home)

python_version = '.'.join(map(str, sys.version_info[:2]))
site_packages = python_home + '/lib/python%s/site-packages' % python_version
site.addsitedir(site_packages)

# Remember original sys.path.

prev_sys_path = list(sys.path)

# Add the site-packages directory.

site.addsitedir(site_packages)

# Reorder sys.path so new directories at the front.

new_sys_path = [ installation_home ]

for item in list(sys.path):
    if item not in prev_sys_path:
        new_sys_path.append(item)
        sys.path.remove(item)

sys.path[:0] = new_sys_path


########################################33

# wsgi does not like monkey patch either
## Detect if we're running via `flask run` and don't monkey patch
#if not os.getenv("FLASK_RUN_FROM_CLI"):
#    from gevent import monkey
#
#    monkey.patch_all()

from CTFd import create_app

app = create_app()

if __name__ == "__main__":
    app.run(debug=True, threaded=True, host="127.0.0.1", port=4000)
else:
    application = app
